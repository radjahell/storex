import React, {useState, useEffect} from 'react';
import { StyleSheet, FlatList , Alert, Pressable, } from 'react-native'; 
 import { Text } from '../components/Themed';
 import {Country} from '../models/country';
 import { StatusBar } from 'expo-status-bar';


export default function TabThreeScreen() {
  const [countriesData, setCountriesData] = useState<Country[]>([]) 

  function fetchCountriesData() {
    fetch('https://restcountries.eu/rest/v2/region/africa?fields=name;capital')
      .then((response) => response.json())
      .then((json) => setCountriesData(json))
      .catch((error) => console.error(error))
  }

  useEffect(()=> {
    fetchCountriesData();
  })

  return (
    <>
      <StatusBar style='light'/>
      <FlatList
        data={countriesData}
        contentContainerStyle={styles.container}
        keyExtractor={item => item.name}
        renderItem={({item})=> <Text onPress={() => {Alert.alert(`The Capital of ${item.name} is ${item.capital}`)}} style={styles.text}>{item.name}</Text>}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30,
    backgroundColor: '#483D8B'
  },
  title: {
    fontSize: 40,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  text: {
    fontSize: 18,
    margin: 5,
    color: '#fff'
  },
});
