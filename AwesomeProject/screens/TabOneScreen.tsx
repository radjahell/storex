import * as React from 'react';
import { StyleSheet } from 'react-native';
import PaymentScreen from '../components/Stripe/PaymentScreen'

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';

export default function TabOneScreen() {
  return (
    <View style={styles.container}> 
      <PaymentScreen />
    </View>
  );
}

const styles = StyleSheet.create({

  container:{
    flex:10, backgroundColor:'grey'
},
 
 
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
